import { Component, OnInit } from '@angular/core';
// @ts-ignore
import data from '../assets/data.json';
import DataInterface from '../assets/interfaces';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  opened: boolean;
  loading = true;
  data: DataInterface[] = data.results;
  categoryes: string[];

  constructor() {
  }

  ngOnInit(): void{
    this.data.map(result => {
      console.log(result);
    });
    setTimeout(() => {
      this.loading = false;
    }, 2500);
  }
}

