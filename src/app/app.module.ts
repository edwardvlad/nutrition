import { BrowserModule } from '@angular/platform-browser';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MaterialModule} from './material/material.module';
import { ToolbarComponent } from './toolbar/toolbar.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { LoadingComponent } from './loading/loading.component';
import { SideMenuComponent } from './side-menu/side-menu.component';
import {RouterModule} from '@angular/router';
import { CalculatorComponent } from './calculator/calculator.component';
import {AppRoutingModule} from './app-routing.module';
import { StepperComponent } from './stepper/stepper.component';
import { IngredientsComponent } from './ingredients/ingredients.component';
import { AmountsComponent } from './amounts/amounts.component';
import { ResultsComponent } from './results/results.component';
import {MatInputModule} from '@angular/material/input';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    LoadingComponent,
    SideMenuComponent,
    CalculatorComponent,
    StepperComponent,
    IngredientsComponent,
    AmountsComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MaterialModule,
    FormsModule,
    RouterModule,
    AppRoutingModule,
    ReactiveFormsModule,
    MatInputModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
