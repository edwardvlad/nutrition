export default interface DataInterface {
  food: string,
  measure: string,
  grams: number,
  calories: number,
  protein: number,
  fat: number,
  satFat: number,
  fiber: number,
  carbs: number,
  category: string
}
